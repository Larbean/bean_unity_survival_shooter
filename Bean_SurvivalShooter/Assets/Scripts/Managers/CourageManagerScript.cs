﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CourageManagerScript : MonoBehaviour {
    public PlayerHealth playerHealth;
    public GameObject courage;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


    void Spawn()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Instantiate(courage, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}