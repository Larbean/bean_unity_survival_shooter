﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;
    Color lightColor = new Color(255, 255, 0, 0.8f);
    float flashSpeed = 5f;
    public PlayerHealth PH;
    AudioSource courageAudio;
    public AudioClip angel;

    public PlayerShooting playerShooting;

    float timeStarted;
     
    
    void Update()
    {
        if (speed <= 12)
        {
            speed += .01f;
        }
        if(Time.time > timeStarted + 60)
        {
            ScoreManager.score += 100;
            timeStarted = Time.time;
        }
    }
    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        courageAudio = GetComponent<AudioSource>();

        timeStarted = Time.time;
    }
    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        Move(h, v);
        Turning();
        Animating(h, v);

    }
     void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);
    }
    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if(Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);

        }
    }
    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);

    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("courage"))
        {
            EnemyHealth[] enemyHealth= FindObjectsOfType<EnemyHealth>();
            foreach(EnemyHealth eM in enemyHealth)
            {
                if (eM.currentHealth >= 16)
                {
                    eM.currentHealth -= 15;
                }
              
            }
            Destroy(other.gameObject);
            courageAudio.clip = angel;
            courageAudio.Play();
            playerShooting.timeBetweenBullets = .15f;
            PH.damageImage.color = lightColor;
            PH.damageImage.color = Color.Lerp(PH.damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
                
        }
    }
}
